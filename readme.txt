Desafio Concrete Version 1.0 26/06/2017

INFORMAÇÕES GERAIS
-------------------

-Desafio proposto pela empresa Concrete Solutions.

-Cadastro de usuário.

-Consulta de usuário.

-Signin.


INFORMAÇÕES DE COMANDOS PARA RODAR O SOFTWARE
----------------------------------------------

npm install  -- Instalar as dependências.
node server  -- Rodar o sistema.
npm run test -- Executar os testes do sistema.
npm run gulp -- Rodar o servidor com validador de javascript em tempo real.



UTILIZAÇÃO DA API
-----------------

link: https://desafio-nodejs.herokuapp.com/v1/usuario
descricao: Rota de cadastro de usuario
method: POST
exemplo:
        { 
            "nome": "teste", 
            "email": "teste@teste.com", 
            "senha": "123", 
            "telefones": [ 
                {
                    "numero": "123456789", 
                    "ddd": "11" 
                    
                } 
            ] 
            
        }


link: https://desafio-nodejs.herokuapp.com/v1/signin
descricao: Rota de signin
method: POST
exemplo: 
        { 
            "email": "diego2@teste.com", 
            "senha": "123"
        }


link: https://desafio-nodejs.herokuapp.com/v1/usuario/:id
descricao: Rota de consulta do usuario
method: GET
header: Authorization   value: Bearer (token)

